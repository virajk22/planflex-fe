import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ProviderDashboardComponent } from "./provider-dashboard/provider-dashboard.component";
import { ProviderRegisterComponent } from "./provider-register/provider-register.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'register',
    component: ProviderRegisterComponent
  },
  {
    path: 'dashboard',
    component: ProviderDashboardComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderRoutingModule { }
