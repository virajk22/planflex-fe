import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientDashboardComponent } from './client-dashboard/client-dashboard.component';
import { ClientRoutingModule } from './client-routing.module';
import { ClientRegisterComponent } from './client-register/client-register.component';

@NgModule({
  declarations: [ClientDashboardComponent, ClientRegisterComponent],
  imports: [CommonModule, ClientRoutingModule],
})
export class ClientModule {}
