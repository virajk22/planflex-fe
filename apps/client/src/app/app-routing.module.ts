import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { Page404Component } from "./pages/page404/page404.component";

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./public/public.module').then(m => m.PublicModule)
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'c',
    loadChildren: () => import('./client/client.module').then(m => m.ClientModule)
  },
  {
    path: 'p',
    loadChildren: () => import('./provider/provider.module').then(m => m.ProviderModule)
  },
  {
    path: '**',
    component: Page404Component
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
