import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProviderDashboardComponent } from './provider-dashboard/provider-dashboard.component';
import { ProviderRoutingModule } from './provider-routing.module';
import { ProviderRegisterComponent } from './provider-register/provider-register.component';

@NgModule({
  declarations: [ProviderDashboardComponent, ProviderRegisterComponent],
  imports: [CommonModule, ProviderRoutingModule],
})
export class ProviderModule {}
