import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ClientDashboardComponent } from "./client-dashboard/client-dashboard.component";
import { ClientRegisterComponent } from "./client-register/client-register.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'register',
    component: ClientRegisterComponent
  },
  {
    path: 'dashboard',
    component: ClientDashboardComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
